/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2012, The GROMACS development team,
 * check out http://www.gromacs.org for more information.
 * Copyright (c) 2012,2013, by the GROMACS development team, led by
 * David van der Spoel, Berk Hess, Erik Lindahl, and including many
 * others, as listed in the AUTHORS file in the top-level source
 * directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

#ifndef GMX_GPU_UTILS_DEVICE_AVERAGES_CUH
#define GMX_GPU_UTILS_DEVICE_AVERAGES_CUH

#include "gromacs/math/gmxcomplex.h"
#include "gromacs/math/vec.h"
#include "gromacs/math/vectypes.h"


__device__ t_complex_d d_cadd_d(t_complex_d a, t_complex_d b);

__device__ t_complex d_cadd(t_complex a, t_complex b);

__device__ t_complex d_cdiff(t_complex a , t_complex b);

__device__ t_complex d_cmul_d(t_complex a, t_complex b);

__device__ static t_complex d_rcmul(real r, t_complex c);

__device__ t_complex_d d_rcmul_d(double r, t_complex_d c);

__device__ t_complex_d d_rcmul(real r, t_complex_d c);

__device__ t_complex_d d_cmul_rd(t_complex a, t_complex_d b);

__device__ real d_r_accum_avg( real sum, real new_value, real fac1, real fac2 );

__device__ double d_accum_avg( double sum, double new_value, double fac1, double fac2 );

__device__ t_complex d_c_accum_avg( t_complex sum, t_complex new_value, real fac1, real fac2 );

__device__ t_complex_d d_cd_accum_avg( t_complex_d sum, t_complex_d new_value, double fac1, double fac2 );

__device__ void d_cvec_accum_avg( cvec *sum, cvec new_value, real fac1, real fac2 );

#endif
